from rockchisel import Builder
import os

path = os.path.dirname(os.path.realpath(__file__))

site = Builder(
	
	input_path  = path,
	output_path = os.path.join(path, "output"),
	theme = "rockchisel.themes.rockdoc",

	variables = dict(version = 1.0),
	page_title_template = "SourceOptics {{ version }}: {{ title }}",
	
	index = "index",

	sections = {
            "About" : {
				"Home" : "index",
                "Use Cases" : "use_cases",
                "FAQ" : "faq"
            },
			"Admin Guide": {
				"Installation": "installation",
                "Admin UI" : "admin",
                "Scanner" : "scanner",
                "Webhooks" : "webhooks",
                "Imports" : "imports",
                "Email Aliases" : "email_alias",
                "Upgrades" : "upgrades",
                "Backups" : "backups",
                "Apache & Nginx" : "apache_nginx",
                "Security" : "security"
			},
            "User Guide" : {
                "Intro" : "user_intro",
                "Organizations" : "organizations",
                "Repos" : "repos", 
                "Repo Index" : "repo_index",
                "Author Index" : "author_index",
                "Date Ranges" : "date_ranges",
                "Repo Stats" : "repo_stats",
                "Author Stats" : "author_stats",
                "Graphs" : "graphs",
                "Commit Feed" : "commit_feed",
                "File Explorer" : "file_explorer",
            },
			"Community": {
				"Contributing": "contributing",
			}
	},
	
	theme_options = dict(
		sidebar_background = "#FF0000"
	)

)

site.build()


